# GNU Radio Docker image
#
# Copyright (C) 2020, 2021, 2022, 2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG GNURADIO_IMAGE_TAG=satnogs
FROM librespace/gnuradio:${GNURADIO_IMAGE_TAG}
LABEL org.opencontainers.image.authors='SatNOGS project <dev@satnogs.org>'

WORKDIR /usr/src

# install build env and required libs etc
COPY packages.builder .
RUN apt -y update && \
    xargs -a packages.builder apt install -qy

# build osdlp-operator
COPY . osdlp-operator/
RUN cd osdlp-operator &&\
    git submodule update --init --recursive &&\
    cmake -B build -DCMAKE_INSTALL_PREFIX=/target/usr -DCMAKE_BUILD_TYPE=Release &&\
    cmake --build build &&\
    cp build/src/osdlp-operator /usr/bin &&\
    cp *.cfg /etc

# build flowgraphs
RUN git clone https://gitlab.com/librespacefoundation/qubik/qubik-comms-sw.git &&\
    cd qubik-comms-sw/test/flowgraphs &&\
    grcc -o /usr/local/bin *.grc &&\
    chmod 0755 /usr/local/bin/*.py

ARG USER_NAME=osdlp
ARG USER_UID=997
ARG USER_HOME=/var/lib/osdlp

# Add unprivileged system user
RUN groupadd -r -g ${USER_UID} ${USER_NAME} \
	&& useradd -r -u ${USER_UID} \
		-g ${USER_NAME} \
		-d ${USER_HOME} \
		-s /bin/bash \
		-G audio,dialout,plugdev \
		${USER_NAME}

# Create application varstate directory
RUN install -d -o ${USER_UID} -g ${USER_UID} ${USER_HOME}

WORKDIR $USER_HOME
USER $USER_NAME

ENTRYPOINT []
CMD ["bash"]

